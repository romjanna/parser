package com.compile.testdemodb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ComponentRel {

    @Id
    @GeneratedValue
    Integer id;

    Integer parentId;

    Integer childId;

}
