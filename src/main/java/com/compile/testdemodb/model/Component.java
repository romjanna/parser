package com.compile.testdemodb.model;

import com.compile.testdemodb.util.ComponentType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Component {

    @Id
    @GeneratedValue
    Integer id;

    String name;

    String fullPath;

    ComponentType type;

}
