package com.compile.testdemodb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class JavaClassInfo {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private String fullPath;

    @JsonIgnore
    @OneToMany(
            mappedBy = "userclass",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private Set<MethodOfClass> methods = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JavaClassInfo that = (JavaClassInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(fullPath, that.fullPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, fullPath);
    }

    @Override
    public String toString() {
        return "JavaClassInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fullPath='" + fullPath + '\'' +
                '}';
    }
}
