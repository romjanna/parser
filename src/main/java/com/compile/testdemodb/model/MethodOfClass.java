package com.compile.testdemodb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MethodOfClass {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private String declaration;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "methodOfClass"
    )
    private List<MethodCallExpression> methodsCalled;

    @ManyToOne
    @JoinColumn(name = "class_method_id", referencedColumnName = "id")
    private JavaClassInfo userclass;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MethodOfClass that = (MethodOfClass) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(declaration, that.declaration) &&
                Objects.equals(userclass, that.userclass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, declaration, userclass);
    }

    @Override
    public String toString() {
        return "MethodOfClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", declaration='" + declaration + '\'' +
                ", userclass=" + userclass +
                '}';
    }
}
