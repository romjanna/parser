package com.compile.testdemodb.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ArtifactScan {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    private String path;

    private Boolean isParsed;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArtifactScan that = (ArtifactScan) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(path, that.path) &&
                Objects.equals(isParsed, that.isParsed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path, isParsed);
    }

}
