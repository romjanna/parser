package com.compile.testdemodb.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MethodCallExpression {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private String declaration;

    private Integer parametrNumber;

    @ManyToOne
    @JoinColumn(name = "base_method_id", referencedColumnName = "id")
    private MethodOfClass methodOfClass;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MethodCallExpression that = (MethodCallExpression) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(declaration, that.declaration) &&
                Objects.equals(parametrNumber, that.parametrNumber) &&
                Objects.equals(methodOfClass, that.methodOfClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, declaration, parametrNumber, methodOfClass);
    }

    @Override
    public String toString() {
        return "MethodCallExpression{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", declaration='" + declaration + '\'' +
                ", parametrNumber=" + parametrNumber +
                ", methodOfClass=" + methodOfClass +
                '}';
    }
}
