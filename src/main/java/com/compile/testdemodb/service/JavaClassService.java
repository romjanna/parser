package com.compile.testdemodb.service;

import com.compile.testdemodb.model.JavaClassInfo;
import com.compile.testdemodb.model.MethodCallExpression;
import com.compile.testdemodb.model.MethodOfClass;
import com.compile.testdemodb.repository.JavaClassInfoRepository;
import com.compile.testdemodb.util.CurrentInfo;
import com.compile.testdemodb.util.FolderScan;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.EnclosedExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.stmt.SwitchEntryStmt;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.stmt.ThrowStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Transactional
public class JavaClassService {

    private final JavaClassInfoRepository repository;

    public Set<MethodOfClass> getMethods(Integer id) {
        try {
            return repository.getOne(id).getMethods();
        } catch (EntityNotFoundException e) {
            e.getCause();
        }
        return null;
    }

    public void saveParceResult(String folderpath) throws IOException {
        if (folderpath.endsWith(".jar")) {
            String destpath = Paths.get(folderpath).getParent().toString().concat("/destgo");
            FolderScan.extractJarArchive(Paths.get(folderpath), Paths.get(destpath));
            listMethodCalls(new File(destpath));
        } else if (folderpath.endsWith(".zip")) {
            String destpath = Paths.get(folderpath).getParent().toString().concat("/destgo");
            FolderScan.extractZipArchive(Paths.get(folderpath), Paths.get(destpath));
            listMethodCalls(new File(destpath));
        } else {
            listMethodCalls(new File(folderpath));
        }
    }

    public void saveData(String file, List<CurrentInfo> ciList) {
        Set<MethodOfClass> setMethods = new HashSet<>();
        ciList.forEach(ci -> setMethods.add(createNewMethod(ci.getMethodDeclaration(), ci.getSubmethods())));
        JavaClassInfo newClass = createNewClass(file, setMethods);
        repository.save(newClass);
    }

    private MethodOfClass createNewMethod(MethodDeclaration method, List<MethodCallExpr> calledMethods) {
        List<MethodCallExpression> calledMethodsObjects = new ArrayList<>();
        calledMethods.forEach(t -> calledMethodsObjects.add(createCallMethodExpression(t)));
        MethodOfClass methodOfClass = new MethodOfClass();
        methodOfClass.setName(method.getName().asString());
        String s = method.getDeclarationAsString(false, false, false);
        methodOfClass.setDeclaration(s.substring(0, s.indexOf('(')));
        calledMethodsObjects.forEach(t -> t.setMethodOfClass(methodOfClass));
        methodOfClass.setMethodsCalled(calledMethodsObjects);
        return methodOfClass;
    }

    private MethodCallExpression createCallMethodExpression(MethodCallExpr methodCallExpr) {
        MethodCallExpression method = new MethodCallExpression();
        method.setName(methodCallExpr.getName().asString());
        NodeList<Expression> params = methodCallExpr.getArguments();
        StringBuilder declString = new StringBuilder("");
        params.forEach(expression -> declString.append(expression.getMetaModel().toString()).append(" "));
        method.setDeclaration(methodCallExpr.toString()); //+ "( " + declString + ")");
        method.setParametrNumber(methodCallExpr.getArguments().size());
        return method;
    }

    public void listMethodCalls(File projectDir) {
        new FolderScan((level, path, file) -> path.endsWith(".java"), (level, path, file) -> {
            System.out.println(path);
            System.out.println(file);
            System.out.println(Strings.repeat("=", path.length()));
            try {
                CompilationUnit cu;
                new VoidVisitorAdapter<Object>() {
                    /**
                     * visit all method declarations in current class with path = path
                     * @param m
                     * @param arg
                     */

                    @Override
                    public void visit(MethodDeclaration m, Object arg) {
                        super.visit(m, arg);
                    }
                }.visit(cu = JavaParser.parse(file), null);

                List<CurrentInfo> ciList = new LinkedList<>();
                getMethodsInClass(cu, ciList);
                saveData(path, ciList);
            } catch (IOException e) {
                new RuntimeException(e);
            }

        }).explore(projectDir);
    }

    private void getMethodsInClass(CompilationUnit compilationUnit,
            List<CurrentInfo> infoList) {
        //List of all methods
        NodeList<TypeDeclaration<?>> types = compilationUnit.getTypes();
        for (TypeDeclaration type : types) {
            List<BodyDeclaration> members = type.getMembers();
            for (BodyDeclaration member : members) {
                if (member instanceof MethodDeclaration) {
                    CurrentInfo localCI = new CurrentInfo();
                    localCI.initialize();
                    MethodDeclaration method = (MethodDeclaration) member;
                    System.out.println("Methods in Class: " + method.getName());
                    //TODO currentInfo.clearData
                    localCI.setMethodDeclaration(method);
                    //scan and save submethods in CurrentInfo object for every method declarared in current class
                    /*
                    if it's not interface, find inside called methods
                     */
                    if (method.getBody().isPresent()) {
                        scanForSubmethodsStmt(method.getBody().get().getStatements(), localCI);
                        infoList.add(localCI);
                    }
                }
            }
        }
    }

    private void scanForSubmethodsStmt(List<Statement> statements, CurrentInfo ci) {
        statements.forEach(stmt -> processStatement(stmt, ci));
    }

    private void scanForSubmethods(List<Node> nodes, CurrentInfo ci) {
        nodes.forEach(n -> processNode(n, ci));
    }

    private boolean processNode(Node expr, CurrentInfo ci) {
        if (expr instanceof Expression) {
            processExpressionStmt((Expression) expr, ci);
        } else if (expr instanceof Statement) {
            processStatement((Statement) expr, ci);
            return true;
        }
        return false;
    }

    private void processEnclosedExpr(EnclosedExpr enclosedExpr, CurrentInfo ci) {
        System.out.println(enclosedExpr);
        enclosedExpr.getChildNodes().forEach(node -> processNode(node, ci));
    }

    private boolean processStatement(Statement stmt, CurrentInfo ci) {
        if (stmt.isExpressionStmt()) {
            processExpressionStmt(stmt.asExpressionStmt().getExpression(), ci);
        } else if (stmt.isSwitchStmt()) {
            processSwitchStmt(stmt.asSwitchStmt(), ci);
            return false;
        } else if (stmt.isThrowStmt()) {
            processThrowStatement(stmt.asThrowStmt(), ci);
            return false;
        } else if (stmt.isWhileStmt()) {
            processWhileStmt(stmt.asWhileStmt(), ci);
        } else if (stmt.isReturnStmt()) {
            if (stmt.asReturnStmt().getExpression().isPresent()) {
                processExpressionStmt(stmt.asReturnStmt().getExpression().get(), ci);
            }
            return false;
        } else if (stmt.isIfStmt()) {
            processIfStmt(stmt.asIfStmt(), ci);
            return false;
        } else if (stmt.isBlockStmt()) {
            processBlockStmt(stmt.asBlockStmt(), ci);
            return false;
        }

        return true;
    }

    private void processThrowStatement(ThrowStmt throwStmt, CurrentInfo ci) {
        List<Node> nodes = throwStmt.getExpression().getChildNodes();
        scanForSubmethods(nodes, ci);
    }

    private void processBlockStmt(BlockStmt stmt, CurrentInfo ci) {
        for (Statement statement : stmt.getStatements()) {
            processStatement(statement, ci);
        }
    }

    private void processIfStmt(IfStmt stmt, CurrentInfo ci) {
        List<Node> nodes = stmt.getChildNodes();
        scanForSubmethods(nodes, ci);
        /* or alternative variant:
        stmt.asIfStmt().getCondition().getChildNodes();
        scanForSubmethods(Collections.singletonList(stmt.asIfStmt().getThenStmt()), ci);
        if(stmt.asIfStmt().getElseStmt().isPresent()) {
            scanForSubmethods(Collections.singletonList(stmt.asIfStmt().getElseStmt().get()), ci);
        }
        */

    }


    private void processWhileStmt(WhileStmt stmt, CurrentInfo ci) {
        List<Node> nodes = stmt.asWhileStmt().getCondition().getChildNodes();
        scanForSubmethods(nodes, ci);
        scanForSubmethods(Collections.singletonList(stmt.asWhileStmt().getBody()), ci);
    }

    /**
     * Handle switch statement block. Here the main execution flow is divided into new subflows.
     */
    private void processSwitchStmt(SwitchStmt switchStmt, CurrentInfo ci) {
        NodeList<SwitchEntryStmt> entries = switchStmt.getEntries();
        entries.forEach(switchEntryStmt -> scanForSubmethodsStmt(switchEntryStmt.getStatements(), ci));
    }

    private void processExpressionStmt(Expression expr, CurrentInfo ci) {
        if (expr.isMethodCallExpr()) {
            System.out.println("Method from processExpressionStmt : " + expr.asMethodCallExpr().toString());
            checkMethodCallExpression(expr.asMethodCallExpr(), ci);
        } else if (expr.isEnclosedExpr()) {
            processNodes(expr.asEnclosedExpr().getChildNodes(), ci);
            System.out.println("Enclosed Expression: " + expr.asEnclosedExpr().toString());
        } else if (expr.isAssignExpr()) {
            System.out.println("Assign Expression: " + expr.asAssignExpr().toString());
            processExpressionStmt(expr.asAssignExpr().getValue(), ci);
            System.out.println("Assign Expression value: " + expr.asAssignExpr().getValue());
        } else if (expr.isVariableDeclarationExpr()) {
            NodeList<VariableDeclarator> variables = expr.asVariableDeclarationExpr()
                    .getVariables();
            for (VariableDeclarator v : variables) {
                if (v.getInitializer().isPresent()) {
                    processExpressionStmt(v.getInitializer().get(),
                            ci);
                }
            }
            System.out.println("Variable Declaration: " + expr);
        } else if (expr.isFieldAccessExpr()) {
            System.out.println("Field Access Expression: " + expr);
        } else if (expr.isBinaryExpr()) {
            processNodes(expr.asBinaryExpr().getChildNodes(), ci);
            System.out.println("Binary Expression: " + expr);
        } else if (expr.isIntegerLiteralExpr()) {
            System.out.println("Integer Expression: " + expr);//StringLiteralExpr
        }
    }

    private void processNodes(List<Node> childNodes, CurrentInfo ci) {
        for (Node n : childNodes) {
            if (n instanceof Expression) {
                processExpressionStmt((Expression) n, ci);
            } else if (n instanceof Statement) {
                processStatement((Statement) n, ci);
            }
        }
    }

    /**
     * iterative function for methods stop than all arguments are not MethodCallExpression else continue check
     * MethodCallExpression arguments
     */

    private void checkMethodCallExpression(MethodCallExpr methodCallExpr, CurrentInfo currentInfo) {
        currentInfo.getSubmethods().push(methodCallExpr);
        if (methodCallExpr.getArguments() == null) {
            return;
        }

        methodCallExpr.getArguments().forEach(expression -> processExpressionStmt(expression, currentInfo));
    }

    /**
     * @param file - full path to the Class
     * @param methods - methods called in the current Class
     * @return new object with Class info
     */
    private JavaClassInfo createNewClass(String file, Set<MethodOfClass> methods) {
        JavaClassInfo javaClassInfo = new JavaClassInfo();
        javaClassInfo.setName(getClassName(file));
        javaClassInfo.setFullPath(file);
        methods.forEach(t -> t.setUserclass(javaClassInfo));
        javaClassInfo.setMethods(methods);
        return javaClassInfo;
    }

    private String getClassName(String fullpath) {
        return fullpath.substring(fullpath.lastIndexOf('/') + 1);
    }

}
