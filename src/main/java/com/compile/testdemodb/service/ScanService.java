package com.compile.testdemodb.service;

import com.compile.testdemodb.model.ArtifactScan;
import com.compile.testdemodb.repository.ArtifactScanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Transactional
public class ScanService {

    private final ArtifactScanRepository repository;

    public boolean checkParced(String path) {
        ArtifactScan artifactScan = repository.findByPath(path);
        if (artifactScan != null) {
            return artifactScan.getIsParsed();
        } else {
            artifactScan = new ArtifactScan();
            artifactScan.setIsParsed(false);
            artifactScan.setPath(path);
            repository.save(artifactScan);
        }
        return false;
    }

    public void setParced(String path, boolean isParsed) {
        ArtifactScan artifactScan = repository.findByPath(path);
        artifactScan.setIsParsed(isParsed);
    }

}
