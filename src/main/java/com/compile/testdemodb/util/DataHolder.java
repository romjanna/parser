package com.compile.testdemodb.util;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.SimpleName;
import com.github.javaparser.ast.stmt.Statement;
import java.util.Stack;

public class DataHolder {

    public MethodDeclaration method;
    public Stack<Statement> flowStatementsStack;
    public NodeList<Statement> flowStatements;

    public DataHolder() {
        
    }

    public DataHolder(CompilationUnit compilationUnit, MethodDeclaration method, SimpleName name) {
    }

    // DataHolder dataHolder = new DataHolder(unit, method, methodName, result)
    //        dataHolder.flowStatements = method.body.get().statements.collect()
}
