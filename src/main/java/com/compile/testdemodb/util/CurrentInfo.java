package com.compile.testdemodb.util;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import java.util.Stack;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CurrentInfo {
    MethodDeclaration methodDeclaration;
    Stack<MethodCallExpr> submethods;

    public void initialize() {
        methodDeclaration = new MethodDeclaration();
        submethods = new Stack<>();
    }
}
