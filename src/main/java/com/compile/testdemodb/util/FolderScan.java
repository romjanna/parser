package com.compile.testdemodb.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FolderScan {

    public interface FileHandler {

        void handle(int level, String path, File file);
    }

    public interface Filter {

        boolean interested(int level, String path, File file);
    }

    private FileHandler fileHandler;
    private Filter filter;

    public FolderScan(Filter filter, FileHandler fileHandler) {
        this.filter = filter;
        this.fileHandler = fileHandler;
    }

    public void explore(File root) {
        explore(0, "", root);
    }

    private void explore(int level, String path, File file) {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                explore(level + 1, path + "/" + child.getName(), child);
            }
        } else {
            if(path.equals("")) {
                path += "/" + file.getName();
            }
            if (filter.interested(level, path, file)) {
                fileHandler.handle(level, path, file);
            }
        }
    }

    public static void extractZipArchive(Path archiveFile, Path destPath) throws IOException {

        deleteDirIfExist(destPath);

        Files.createDirectories(destPath); // create dest path folder(s)

        try (ZipFile archive = new ZipFile(archiveFile.toFile())) {

            // sort entries by name to always create folders first
            List<? extends ZipEntry> entries = archive.stream()
                    .sorted(Comparator.comparing(ZipEntry::getName))
                    .collect(Collectors.toList());

            // copy each entry in the dest path
            for (ZipEntry entry : entries) {
                Path entryDest = destPath.resolve(entry.getName());

                if (entry.isDirectory()) {
                    Files.createDirectory(entryDest);
                    continue;
                }

                Files.copy(archive.getInputStream(entry), entryDest);
            }
        }
    }

    public static void extractJarArchive(Path archiveFile, Path destPath) throws IOException {

        deleteDirIfExist(destPath);

        Files.createDirectories(destPath); // create dest path folder(s)

        try (JarFile archive = new JarFile(archiveFile.toFile())) {

            // sort entries by name to always create folders first
            List<? extends JarEntry> entries = archive.stream()
                    .sorted(Comparator.comparing(JarEntry::getName))
                    .collect(Collectors.toList());

            // copy each entry in the dest path
            for (JarEntry entry : entries) {
                Path entryDest = destPath.resolve(entry.getName());

                if (entry.isDirectory()) {
                    Files.createDirectory(entryDest);
                    continue;
                }

                Files.copy(archive.getInputStream(entry), entryDest);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Not found .java files in project " + archiveFile);
        }
    }

    private static void deleteDirIfExist(Path destPath) throws IOException {
        if (Files.exists(destPath)) {
            Files.walk(destPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

    }
