package com.compile.testdemodb.repository;

import com.compile.testdemodb.model.ArtifactScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtifactScanRepository extends JpaRepository<ArtifactScan, Integer> {

    ArtifactScan findByPath(String path);

}
