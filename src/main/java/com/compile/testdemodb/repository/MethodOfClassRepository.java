package com.compile.testdemodb.repository;

import com.compile.testdemodb.model.MethodOfClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MethodOfClassRepository extends JpaRepository<MethodOfClass, Integer> {

}
