package com.compile.testdemodb.repository;

import com.compile.testdemodb.model.JavaClassInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JavaClassInfoRepository extends JpaRepository<JavaClassInfo, Integer> {

}

