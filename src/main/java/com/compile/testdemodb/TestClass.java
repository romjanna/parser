package com.compile.testdemodb;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestClass {

    public static void main(String[] args) {
        System.out.println("Test for recurtion " + Math.random());
        double i = 1;

        double res;
        while ((res = Math.pow(2, i++)) < Double.MAX_VALUE) {
            if (((int) res) % 10 == Math.random() % 10) {
                break;
            }
        }

        log.info("Output> {}", String.format("Final value: %s", res));
    }

    private static int methodToCall() {
        SecurityManager r = System.getSecurityManager();
        // HD 2-12 Overflow iff both arguments have the opposite sign of the result
        if (r.hashCode() > Integer.MAX_VALUE) {
            throw new ArithmeticException("integer overflow" + r.hashCode());
        } else {
            System.out.println("Something to print " + Math.random());
        }
        return Integer.valueOf("7");
    }
}


/*
Math.pow
Math.random
String.format
log.info

 */