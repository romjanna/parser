package com.compile.testdemodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestdemodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestdemodbApplication.class, args);
    }
}
