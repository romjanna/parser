package com.compile.testdemodb.controller;

import com.compile.testdemodb.service.JavaClassService;
import com.compile.testdemodb.service.ScanService;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
class HomePageController {

    private static final String PROJECT_1 = "/Users/janna.silonosova/IdeiaProjects/javaparser-visited/src/main/java/org/javaparser/examples/chapter5";

    private static final String PROJECT_2 = "/Users/janna.silonosova/IdeiaProjects/" + //parsetodb";
            "testdemodb/src/main/java/com/compile/testdemodb/TestClass.java";

    private static final String PROJECT_3 = "/Users/janna.silonosova/IdeiaProjects/" + //parsetodb";
            "testdemodb";

    private static final String PROJECT_4 = "/Users/janna.silonosova/" + //parsetodb";
            "spring-boot-cli-2.1.1.RELEASE.jar";

    private static final String PROJECT_5 = "/Users/janna.silonosova/IdeiaProjects/spring-boot";


    private final JavaClassService service;

    private final ScanService scanService;

    @PostMapping("/parce")
    void saveParceResult() {
        parseAndSave(PROJECT_2);
    }

    private void parseAndSave(String projectPath) {
        if(!scanService.checkParced(projectPath)) { // create if not exist
            try {
                service.saveParceResult(projectPath);
                //set parced
                scanService.setParced(projectPath, true);
            } catch (IOException e) {
                //set not parced
                e.printStackTrace();
                scanService.setParced(projectPath, false);
            }
        } else {
            System.out.println("Was parced before");
        }
    }
}

