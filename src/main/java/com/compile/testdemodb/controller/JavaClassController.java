package com.compile.testdemodb.controller;

import com.compile.testdemodb.model.MethodCallExpression;
import com.compile.testdemodb.model.MethodOfClass;
import com.compile.testdemodb.service.JavaClassService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(JavaClassController.BASE_URL)
class JavaClassController {

    public static final String BASE_URL = "/methods";

    private final JavaClassService javaClassService;

    @GetMapping("/{id}")
    Set<MethodOfClass> getMethodsForClass(@PathVariable Integer id) {
        return javaClassService.getMethods(id);
    }

    /**
     * @param id id of java Class
     * @param mid id of analizing method
     */
    @GetMapping("/{id}/{mid}")
    List<MethodCallExpression> getMethodExpressionsOfMethodWithId(@PathVariable Integer id,
            @PathVariable Integer mid) {
        return javaClassService.getMethods(id).stream().filter(m -> m.getId().equals(mid)).findFirst().get()
                .getMethodsCalled();
    }
}
